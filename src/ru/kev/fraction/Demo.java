package ru.kev.fraction;


import java.util.Scanner;
import java.util.regex.*;

/**
 * Класс для проверки работоспособности класса Fraction.
 *
 * @author Карнаухов Евгений 15ОИТ18.
 */

public class Demo {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] strings = new String[3];
        Fraction fraction = new Fraction();
        Fraction fraction1 = new Fraction();
        write(strings);
        splitarray(0, strings, fraction);
        splitarray(2, strings, fraction1);
        System.out.println(fraction +" " + strings[1]+" "+ " " + fraction1 + " = " + math(fraction, fraction1, strings[1]));


    }

    /**
     * Метод,в котором вводится строка и который разделяет её на 3 части
     * и отправляет в другой метод для сохранения их в массив .
     *
     * @param strings массив для хранения частей строки
     */
    private static void write(String[] strings) {
        String string;
        boolean value;
        do {
            string = scanner.nextLine();
            value = checkup(string);
            if (value == false) {
                System.out.println("Некорректно введены исходные данные, попробуйте еще раз: ");
            }
        } while (value == false);
        int i = 0;
        for (String retval : string.split(" ")) {
            savearray(i, strings, retval);
            i++;
        }
    }

    private static boolean checkup(String string) {
        Pattern pattern = Pattern.compile("[1-9]+[0-9]*[/][1-9]+[0-9]*\\s[+]*[-]*[*]*[/]*\\s[1-9]+[0-9]*[/][1-9]+[0-9]*");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    /**
     * Метод, который сохраняет часть разделенной строки в массив.
     *
     * @param i       номер элемента массива
     * @param strings массив
     * @param retval  часть разделенной строки
     */
    private static void savearray(int i, String[] strings, String retval) {
        strings[i] = retval;

    }

    /**
     * Метод, который разделяет строку на числитель и знаменатель и сохраняет их в обьекте Fraction.
     *
     * @param i        номер элемента массива
     * @param strings  массив
     * @param fraction обьект
     */
    private static void splitarray(int i, String[] strings, Fraction fraction) {
        String[] strings1 = new String[2];
        int i1 = 0;
        for (String retval : strings[i].split("/")) {
            savearray(i1, strings1, retval);
            i1++;
        }
        fraction.setNum(Integer.parseInt(strings1[0]));
        fraction.setDenum(Integer.parseInt(strings1[1]));
    }

    public static Fraction math(Fraction fraction, Fraction fraction1, String string) {
        Fraction fraction2 = new Fraction();
        switch (string) {
            case "+":
                fraction2 = fraction.summa(fraction1);
                return fraction2;
            case "-":
                fraction2 = fraction.difference(fraction1);
                return fraction2;
            case "/":
                fraction2 = fraction.div(fraction1);
                return fraction2;
            case "*":
                fraction2 = fraction.multiplication(fraction1);
                return fraction2;
        }
        return null;
    }
}