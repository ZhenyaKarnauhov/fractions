package ru.kev.fraction;


/**
 * Класса Fraction.
 *
 * @author Карнаухов Евгений 15ОИТ18.
 */



public class Fraction {
    private int num;
    private int denum;

    public Fraction(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }


    public Fraction(int num) {
        this(num, 1);
    }

    public Fraction() {
        this(1, 1);
    }

    public int getNum() {
        return num;
    }

    public int getDenum() {
        return denum;
    }

    @Override
    public String toString() {
        return num + "/" + denum;
    }


    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) {
        this.denum = denum;
    }

    public Fraction summa(Fraction fraction1) {
        Fraction fraction2 = new Fraction();
        if (this.denum > fraction1.denum && this.denum % fraction1.denum == 0) {
            fraction1.num = (this.denum / fraction1.denum) * fraction1.num;

            fraction2 = new Fraction(fraction1.num + this.num, this.denum);
            return fraction2;
        }
        if (this.denum == fraction1.denum) {

            fraction2 = new Fraction(fraction1.num + this.num, this.denum);
            return fraction2;
        }
        if (this.denum > fraction1.denum && this.denum % fraction1.denum != 0) {
            this.num = (this.denum * fraction1.denum) / this.denum * this.num;
            fraction1.num = (this.denum * fraction1.denum) / fraction1.denum * fraction1.num;

            fraction2 = new Fraction(fraction1.num + this.num, fraction1.denum * this.denum);
            return fraction2;
        }
        if (fraction1.denum > this.denum && fraction1.denum % this.denum != 0) {
            this.num = (fraction1.denum * this.denum) / this.denum * this.num;
            fraction1.num = (fraction1.denum * this.denum) / fraction1.denum * fraction1.num;

            fraction2 = new Fraction(fraction1.num + this.num, fraction1.denum * this.denum);
            return fraction2;
        }
        if (fraction1.denum > this.denum && fraction1.denum % this.denum == 0) {
            this.num = (fraction1.denum / this.denum) * this.num;

            fraction2 = new Fraction(this.num + fraction1.num, fraction1.denum);
            return fraction2;

        }
        return fraction2;
    }

    public Fraction difference(Fraction fraction1) {
        Fraction fraction4 = new Fraction();
        if (this.denum == fraction1.denum) {
            fraction4 = new Fraction(this.num - fraction1.num, this.denum);
            return fraction4;
        }
        if (this.denum > fraction1.denum && this.denum % fraction1.denum != 0) {
            this.num = (this.denum * fraction1.denum) / this.denum * this.num;
            fraction1.num = (this.denum * fraction1.denum) / fraction1.denum * fraction1.num;

            fraction4 = new Fraction(this.num - fraction1.num, this.denum * fraction1.denum);
            return fraction4;
        }
        if (fraction1.denum > this.denum && fraction1.denum % this.denum == 0) {
            this.num = (fraction1.denum / this.denum) * this.num;

            fraction4 = new Fraction(this.num - fraction1.num, fraction1.denum);
            return fraction4;
        }
        if (this.denum > fraction1.denum && this.denum % fraction1.denum == 0) {
            fraction1.num = (this.denum / fraction1.denum) * fraction1.num;

            fraction4 = new Fraction(this.num - fraction1.num, this.denum);
            return fraction4;
        }
        if (fraction1.denum > this.denum && fraction1.denum % this.denum != 0) {
            this.num = (fraction1.denum * this.denum) / this.denum * this.num;
            fraction1.num = (fraction1.denum * this.denum) / fraction1.denum * fraction1.num;

            fraction4 = new Fraction(this.num - fraction1.num, fraction1.denum * this.denum);
            return fraction4;
        }
        return fraction4;
    }


    public Fraction div(Fraction fraction1) {
        Fraction fraction = new Fraction(this.num * fraction1.denum, this.denum * fraction1.num);
        return fraction;
    }

    public Fraction multiplication(Fraction fraction2) {
        Fraction fraction3 = new Fraction(this.num * fraction2.num, this.denum * fraction2.denum);
        return fraction3;

    }
}


